# ENGGEN 403 T-rol-ling #

Full credit to https://lastminuterol.appspot.com for the idea.

Instead of using Markov Chains, this project relies on sentence templates. This gives tighter control over the sentences, but the results can be a little less funny.

The project works by asking for a few key subjects to write about, then harnesses the amazing power of Google to grab a few related words to shove into randomly-selected sentence templates. There are different sentence templates for introduction, middle, and concluding sentences in paragraphs. It can write more than one paragraph about a subject by writing follow-up paragraphs using a popular keyword as the basis. The project always tries to write five paragraphs of 2 to 5 sentences each. It uses pdflatex to generate a pdf file ready for submission.

### How do I get set up? ###

* Clone it
* Install pdflatex and python3
* ./t-rol-ling.py
* Follow the prompts

### Who do I talk to? ###

* Me