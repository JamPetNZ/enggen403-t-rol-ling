#!/usr/bin/env python3
# If this smells hacked-together, that's because I came up with it as I went along

import datetime, http.client, math, os, random, shlex, subprocess, sys

LOCAL_DIR = os.path.abspath(os.path.dirname(sys.argv[0]))

def stripTags(text, tags=("<", ">")):
    outText = []

    tagPos = 0
    while True:
        nextTag = text.find(tags[0], tagPos)
        if nextTag == -1:
            outText.append(text[tagPos:])
            break

        outText.append(text[tagPos:nextTag])
        nextTagEnd = text.find(tags[1], nextTag + len(tags[0]))
        if nextTagEnd == -1:
            break
        
        tagPos = nextTagEnd + len(tags[1])

    return "".join(outText)

class KeywordFilter:
    def __init__(self):
        self.functionWords = self.getFunctionWords()

    def isFunctionWord(self, word):
        return word.lower() in self.functionWords

    def getFunctionWords(self):
        functionWords = set()
        for line in open(os.path.join(LOCAL_DIR, "deps/functionwords.txt"), "r"):
            functionWords.add(line.lower().strip())

        return functionWords
        

class URLEncoder:
    def __init__(self):
        pass

    def makeURLEscaped(self, rawText):
        urlStr = ""
        for rawChar in rawText:
            rawCharOrd = ord(rawChar)
            needEscape = True

            if rawCharOrd >= 65 and rawCharOrd <= 90: # Uppercase
                needEscape = False
            elif rawCharOrd >= 97 and rawCharOrd <= 122: # Lowercase
                needEscape = False
            elif rawCharOrd >= 48 and rawCharOrd <= 57: # Numbers
                needEscape = False
            elif rawChar in "*-._": # Others
                needEscape = False

            if needEscape:
                urlStr += "%%%02x" % (rawCharOrd)
            else:
                urlStr += rawChar

        return urlStr

    def makeURLParameters(self, rawParams):
        parameterStrings = []

        for rawKey in sorted(rawParams):
            rawVal = rawParams[rawKey]
            
            strKey = self.makeURLEscaped(rawKey)
            strVal = self.makeURLEscaped(rawVal)

            parameterStrings.append(strKey + "=" + strVal)

        return "&".join(parameterStrings)

class Browser:
    def __init__(self):
        pass

    def getHTTPS(self, urlStr):
        urlStrLower = urlStr.lower()
        if urlStrLower.startswith("https://"):
            hostNameStart = 8
        else:
            hostNameStart = 0
        
        hostNameEnd = urlStrLower.find("/", hostNameStart)
        if hostNameEnd == -1:
            hostName = urlStrLower[hostNameStart:]
        else:
            hostName = urlStrLower[hostNameStart:hostNameEnd]
        
        conn = http.client.HTTPSConnection(hostName)
        conn.request("GET", urlStr)
        return str(conn.getresponse().read())

class Google:
    def __init__(self):
        self.urlEncoder = URLEncoder()
        self.browser = Browser()
        self.kwFilter = KeywordFilter()

    def makeURL(self, searchText):
        urlParams = {}
        urlParams["q"] = searchText
        urlText = self.urlEncoder.makeURLParameters(urlParams)
        
        return "https://www.google.com/search?" + urlText

    def getSearchPage(self, searchText):
        return self.browser.getHTTPS(self.makeURL(searchText))

    def getCleanText(self, text):
        outText = stripTags(text)
        outText = stripTags(outText, ("&", ";"))
        outText = stripTags(outText, ("[", "]"))
        outText = stripTags(outText, ("{", "}"))
        outText = outText.replace("\\n", " ")
        
        return outText

    def getSearchResults(self, searchText):
        results = []
        resultsRaw = self.getSearchPage(searchText)

        sigResHeadBeg = "<h3 class=\"r\">"
        sigResTextBeg = "<span class=\"st\">"

        resultPos = 0
        while True:
            # Get the header
            resultPos = resultsRaw.find(sigResHeadBeg, resultPos)
            if resultPos == -1:
                break
            resultPos += len(sigResHeadBeg)
            resultHead = resultsRaw[resultPos:resultsRaw.find("</h3>", resultPos)]

            # Get the text start
            resultTextPos = resultsRaw.find(sigResTextBeg, resultPos)
            if resultTextPos == -1:
                continue
            resultTextPos += len(sigResTextBeg)
            
            balance = 1
            resultTextEnd = resultTextPos
            while balance != 0:
                nextSpanOpen = resultsRaw.find("<span",   resultTextEnd+1)
                nextSpanClose = resultsRaw.find("</span", resultTextEnd+1)

                if nextSpanOpen == -1:
                    closeFirst = True
                elif nextSpanClose == -1:
                    closeFirst = False
                else:
                    closeFirst = nextSpanClose < nextSpanOpen

                if closeFirst:
                    balance -= 1
                    resultTextEnd = nextSpanClose
                else:
                    balance += 1
                    resultTextEnd = nextSpanOpen

                if resultTextEnd == -1:
                    resultTextEnd = resultTextPos
                    break

            resultText = resultsRaw[resultTextPos:resultTextEnd]

            cleanHead = self.getCleanText(resultHead)
            cleanText = self.getCleanText(resultText)
            results.append((cleanHead, cleanText))

        return results
    
    def getKeywords(self, searchTextRaw):
        searchText = searchTextRaw.lower()
        results = self.getSearchResults(searchText)
        kwFreq = {}
        kwCapital = {}

        forceLower = True

        for res in results:
            for word in res[1].split():
                for repChar in ".,_()[]{}~!?:;\"'\\":
                    word = word.replace(repChar, "")
                wordLower = word.lower()

                if len(wordLower) < 2: continue # Too short or empty
                if self.kwFilter.isFunctionWord(wordLower): continue
                #if wordLower in searchText: continue # Trivial keyword

                # Check for "words" without letters
                containsAlpha = False
                for wordChar in wordLower:
                    if ord(wordChar) >= 97 and ord(wordChar) <= 122:
                        containsAlpha = True
                        break
                if not containsAlpha: continue

                if wordLower in kwFreq:
                    kwFreq[wordLower] += 1
                    kwCapital[wordLower] &= (wordLower != word)
                    
                else:
                    kwFreq[wordLower] = 1
                    kwCapital[wordLower] = (wordLower != word)

        freqSorted = sorted(kwFreq, key=kwFreq.get, reverse=True)
        freqSorted = [[keyStr, kwFreq[keyStr]] for keyStr in freqSorted]
        for i in range(len(freqSorted)):
            keyStr = freqSorted[i][0]
            if kwCapital[keyStr] and not forceLower:
                freqSorted[i][0] = keyStr.capitalize()
        
        return freqSorted

class SentenceGen:
    def __init__(self):
        self.templates = self.getTemplates()
        self.templatesFresh = {}
        for category in self.templates:
            self.templatesFresh[category] = []

    def getTemplates(self):
        templates = {}
        for line in open(os.path.join(LOCAL_DIR, "deps/sentencetemplates.txt"), "r"):
            lineSplit = line.strip().split()
            if len(lineSplit) < 2:
                continue

            for lineUsage in lineSplit[0].lower():
                if lineUsage not in templates:
                    templates[lineUsage] = []
                
                templates[lineUsage].append(" ".join(lineSplit[1:]))

        return templates

    def getFreshTemplate(self, sentType):
        templatesToChoose = self.templatesFresh[sentType]

        if len(templatesToChoose) == 0:
            self.templatesFresh[sentType] = [sentTemp for sentTemp in self.templates[sentType]]
            templatesToChoose = self.templatesFresh[sentType]

        template = random.choice(templatesToChoose)
        templatesToChoose.remove(template)

        return template


    # "Word bags" consist of pairs of [word, wordPriority]
    def genSentences(self, wordBag, nSentences=1):
        sentences = []
        categories = ["+", "-"]

        wordCats = {}
        for cat in categories: wordCats[cat] = []
        
        for wordEntry in wordBag:
            category = random.choice(categories)
            wordCats[category].append(wordEntry[0])

            '''for i in range(wordEntry[1]):
                wordCats[category].append(wordEntry[0])
            random.shuffle(wordCats[category])'''

        for sNum in range(nSentences):
            sentence = []
            if sNum == 0:
                sentType = "i"
            elif sNum == nSentences - 1:
                sentType = "c"
            else:
                sentType = "m"

            if sentType not in self.templates:
                sentType = "m"
                
            template = self.getFreshTemplate(sentType)

            tempPos = 0
            while True:
                nextTempPos = template.find("#", tempPos)
                if nextTempPos == -1:
                    sentence.append(template[tempPos:])
                    break
                
                sentence.append(template[tempPos:nextTempPos])
                repCat = "+"
                repCap = False
                repTyp = "n"
                repSig = False
                repPlr = False
                repNumStr = ""
                nextTempEnd = nextTempPos
                while True:
                    nextTempEnd += 1
                    if (nextTempEnd >= len(template)):
                        break

                    typeChar = template[nextTempEnd]

                    if typeChar in "+-":
                        repCat = typeChar
                    elif typeChar in "su":
                        repSig = typeChar == "s"
                    elif typeChar in "p":
                        repPlr = True
                    elif typeChar in "nva":
                        repTyp = typeChar
                        repCap = False
                    elif typeChar in "NVA":
                        repTyp = typeChar.lower()
                        repCap = True
                    elif typeChar in "0123456789":
                        repNumStr += typeChar
                    else:
                        if typeChar in "_":
                            nextTempEnd += 1
                        break

                wordList = wordCats[repCat]
                if repNumStr == "":
                    repNum = int(random.uniform(0, len(wordList)))
                else:
                    repNum = int(repNumStr)
                tempPos = nextTempEnd

                if len(wordList) == 0:
                    word = "thing"
                else:
                    if repSig:
                        word = wordList[repNum % len(wordList)]
                    else:
                        word = random.choice(wordList)

                if repCap:
                    word = word.capitalize()

                if repPlr:
                    if word.endswith("ss"):
                        word = word + "es"
                    elif word.endswith("sis"):
                        word = word[:-3] + "ses"
                    elif word.endswith("s"):
                        pass
                    elif word.endswith("y"):
                        word = word[:-1] + "ies"
                    else:
                        word = word + "s"
                else:
                    if word.endswith("ss"):
                        pass
                    elif word.endswith("sis"):
                        pass
                    elif word.endswith("species"):
                        pass
                    elif word.endswith("series"):
                        pass
                    elif word.endswith("ies"):
                        word = word[:-3] + "y"
                    elif word.endswith("sses"):
                        word = word[:-2]
                    elif word.endswith("ses"):
                        word = word[:-3] + "sis"
                    elif word.endswith("s"):
                        word = word[:-1]
                sentence.append(word)

            sentences.append("".join(sentence))

        return " ".join(sentences)

def getROLFullTex(userName, userUPI, userSID, docText):
    with open(os.path.join(LOCAL_DIR, "deps/roltemplate.tex"), "r") as f:
        texTemplate = f.read()

    texOutput = texTemplate
    texOutput = texOutput.replace("#USER_NAME#", userName)
    texOutput = texOutput.replace("#USER_UPI#", userUPI)
    texOutput = texOutput.replace("#USER_SID#", userSID)
    texOutput = texOutput.replace("#DOC_TEXT#", docText)

    return texOutput

if __name__ == "__main__":
    google = Google()
    sGen = SentenceGen()

    userName = input("Enter your full name: ")
    userUPI = input("Enter your UPI: ")
    userSID = input("Enter your student ID: ")

    #subjects = ["Equity", "Investment", "University Assignments"]
    #subjects = ["Trading", "Innovation", "Financing"]
    subjects = shlex.split(input("What subjects should we cover? "))
    nParas = 5
    relatednessLimit = 3

    nParasWritten = 0
    paras = {}
    random.shuffle(subjects)
    for subjectNum in range(len(subjects)):
        baseSubject = subjects[subjectNum]
        subject = baseSubject
        nToDo = int(math.ceil((nParas - nParasWritten) / (len(subjects) - subjectNum)))

        paras[baseSubject] = []
        for i in range(nToDo):
            kw = google.getKeywords(subject)
            nSents = round(random.uniform(2, 5))
            paras[baseSubject].append(sGen.genSentences(kw, nSents))
            nParasWritten += 1

            subject = kw[math.floor(random.uniform(0, min(len(kw), relatednessLimit)))][0]

    assembledText = []
    for paraSubj in paras:
        assembledPara = []
        assembledPara.append("\\section*{%s}\n" % (paraSubj.capitalize()))

        assembledPara.append("\\\\\n\n".join(paras[paraSubj]))

        assembledText.append("".join(assembledPara))

    docFolder = os.path.abspath(str(datetime.date.today()))
    if not os.path.exists(docFolder):
        os.mkdir(docFolder)

    dirWorking = os.getcwd()
    os.chdir(docFolder)
    docFNameTex = userSID + ".tex"
    with open(docFNameTex, "w") as f:
        f.write(getROLFullTex(userName, userUPI, userSID, "\n\n".join(assembledText)))
    p = subprocess.Popen(["pdflatex", "\"" + docFNameTex + "\""])
    p.wait()
    os.chdir(dirWorking)
